
<?php
  // log connections

  $LOG_FILE = "../logs/logs.txt";

  // create the line to log
  $line =  date("Y-m-d_H:i:s") . "\t" . $_SERVER["REMOTE_ADDR"] . "\t(" . $_SERVER["REMOTE_PORT"] . ")\t";
  // add localization if available (use API http://ip-api.com)
  $clientData = json_decode(file_get_contents("http://ip-api.com/json/" . $_SERVER['REMOTE_ADDR']), true);
  if (isset($clientData["country"])) {
    $line = $line . $clientData["country"] . "\t" . $clientData["regionName"] . "\t";
    $line = $line . $clientData["city"];
  }
  $line = $line . $_SERVER["REQUEST_URI"] . "\n";

  // open file
  $logFile = fopen($LOG_FILE, "a");

  // write and close the file
  fputs($logFile, $line);
  fclose($logFile);

?>
